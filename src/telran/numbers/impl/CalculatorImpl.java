package telran.numbers.impl;

import telran.numbers.ICalculator;

public class CalculatorImpl implements ICalculator {

    @Override
    public int sum(int a, int b) {

        return a + b;
    }

    @Override
    public int subtract(int a, int b) {

        return a - b;
    }

    @Override
    public int divide(int a, int b) {

        return a / b;
    }

    @Override
    public int multiply(int a, int b) {

        return a * b;
    }

}
