package telran.numbers.controller;

import telran.numbers.ICalculator;
import telran.numbers.net.CalculatorTcpProxy;

import java.util.Scanner;

public class CalculatorApplClient {

    private static final String HOST = "localhost";
    private static final int PORT = 9001;

    public static void main(String[] args) {
        ICalculator calculator = new CalculatorTcpProxy(HOST, PORT);
        Scanner scanner = new Scanner(System.in);
        RunScan(calculator, scanner);

    }

    private static void RunScan(ICalculator calculator, Scanner scanner) {
        while (true) {
            try {
                System.out.println("Enter operation [+,/,*,-] or exit");
                String operation = scanner.nextLine();
                if (operation.equalsIgnoreCase("exit")) {
                    break;
                }
                System.out.println("Enter first number");

                int num1 = scanner.nextInt();
                System.out.println("Enter second number");

                int num2 = scanner.nextInt();
                int res = 0;

                switch (operation) {
                    case "+" -> res = calculator.sum(num1, num2);
                    case "-" -> res = calculator.subtract(num1, num2);
                    case "*" -> res = calculator.multiply(num1, num2);
                    case "/" -> res = calculator.divide(num1, num2);
                    default -> {
                        System.out.println("wrong operation");
                        continue;
                    }
                }
                System.out.println(res);
            } catch (Exception e) {
                System.out.println("input error " + e.getMessage());

            }

        }
    }


}
