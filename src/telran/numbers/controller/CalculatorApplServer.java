package telran.numbers.controller;

import telran.numbers.net.CalculatorTcpListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class CalculatorApplServer {
    private static final int PORT = 9001;
    private static ServerSocket serverSocket;

    public static void main(String[] args) throws IOException {
        serverSocket = new ServerSocket(PORT);
        System.out.println("Socket was launched on the port " + PORT);
        while (true) {
            Socket socket = serverSocket.accept();
            CalculatorTcpListener.runListener(socket);
        }

    }

}