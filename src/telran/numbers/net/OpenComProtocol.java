package telran.numbers.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class OpenComProtocol {

    private static final int PORT = 4000;
    private static final String serverVersion = "0.1";
    private static final String[] serverCommands = {"START", "QUIT"};

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(PORT);
        System.out.println("Server is listening on port " + PORT);
        while (true) {
            Socket socket = serverSocket.accept();
            runClient(socket);
        }

    }

    public static void runClient(Socket socket) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintStream writer = new PrintStream(socket.getOutputStream())) {
            while (true) {
                writer.println("TCP Server version: " + serverVersion);
                String request = reader.readLine();
                if (request == null) {
                    break;
                }
                String response = getResponse(request, socket);
                writer.println(response);
            }
        } catch (IOException e) {
            // client closed connection by any illegal way
            System.out.println("illegal way of closing connection");
            return;
        }
        System.out.println("client closed connection");
        return;
    }

    private static String getResponse(String request, Socket socket) throws IOException {

        // request: <headers># <payload

        String[] headersPayload = request.split("#");
        if (headersPayload.length != 2) {
            return "Unknown request";
        }
        String headers = headersPayload[0];
        String payload = headersPayload[1];

        switch (headers) {
            case "EHLO":
                return getCommandList(payload, socket);
            case "START":
                return null;
            default:
                return "Unknown Request";
        }
    }

    private static String getLength(String payload) {

        return Integer.toString(payload.length());
    }

    public static String getCommandList(String payload, Socket socket) throws IOException {
        PrintStream writer = new PrintStream(socket.getOutputStream());
        writer.println("Hello " + payload);
        Arrays.stream(serverCommands).forEach(writer::println);

        return null;
    }

}
