package telran.numbers.net;

import java.io.Serializable;

public class ResponseCalculator implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public String code; //success, error
    public String result; //if success - number, if error - exception message

    public ResponseCalculator(String code, String result) {
        super();
        this.code = code;
        this.result = result;
    }


}
