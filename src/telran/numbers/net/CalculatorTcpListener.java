package telran.numbers.net;

import telran.numbers.impl.CalculatorImpl;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


public class CalculatorTcpListener {


    public static void runListener(Socket socket) {

        {

            try (ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                 ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())
            ) {


                while (true) {

                    RequestCalculator requestData = (RequestCalculator) in.readObject();
                    ResponseCalculator res = serverOperations(requestData);

                    out.writeObject(res);
                }
            } catch (Exception e) {
                System.out.println("Client closed connection" + e.getMessage());
            }

        }

    }


    private static ResponseCalculator serverOperations(RequestCalculator request) {
        CalculatorImpl calc = new CalculatorImpl();
        String type = request.type;
        Integer a = request.numbers[0];
        Integer b = request.numbers[1];
        String code = "success";

        String result = "";

        switch (type) {
            case "-" -> result = Integer.toString(calc.subtract(a, b));
            case "+" -> result = Integer.toString(calc.sum(a, b));
            case "*" -> result = Integer.toString(calc.multiply(a, b));
            case "/" -> {
                if (b.equals(0)) {
                    result = "DEV to by zero";
                    code = "error";
                } else {
                    result = Integer.toString(calc.divide(a, b));
                }
            }
        }
        return new ResponseCalculator(code, result);
    }
}
