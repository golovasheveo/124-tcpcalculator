package telran.numbers.net;

import telran.numbers.ICalculator;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class CalculatorTcpProxy implements ICalculator {
    Socket socket;
    ObjectOutputStream writer;
    ObjectInputStream reader;

    public CalculatorTcpProxy(String host, int port) {
        try {
            socket = new Socket(host, port);
            writer = new ObjectOutputStream(socket.getOutputStream());
            reader = new ObjectInputStream(socket.getInputStream());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public int sum(int a, int b) {
        String type = "+";
        return sendCalculatorRequest(a, b, type);

    }

    private int sendCalculatorRequest(int a, int b, String type) {
        try {
            writer.writeObject(new RequestCalculator(type, new Integer[]{a, b}));
            ResponseCalculator response = (ResponseCalculator) reader.readObject();
            if (response.code.equals("success")) {
                return Integer.parseInt(response.result);
            } else {
                throw new RuntimeException(response.result);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public int subtract(int a, int b) {
        String type = "-";
        return sendCalculatorRequest(a, b, type);
    }

    @Override
    public int divide(int a, int b) {
        String type = "/";
        return sendCalculatorRequest(a, b, type);
    }

    @Override
    public int multiply(int a, int b) {
        String type = "*";
        return sendCalculatorRequest(a, b, type);
    }

}
